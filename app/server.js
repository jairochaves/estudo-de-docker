require("dotenv").config();
const express = require("express");
const db = require("./db");

const app = express();
const port = 3000;

app.get("/", (req, res) => {
  res.json({ message: "Hello DIRIN" });
});
app.get("/dev", (req, res) => {
  res.json({ message: "Hello DIRIN" });
});
app.get("/dev/insert/:name_insert", (req, res) => {
  const { name_insert } = req.params;
  console.log("aqui");
  if (!name_insert) {
    console.log("aqui 2");

    return res.status(400).send();
  }
  // console.log(db);
  const sql = "insert into devs (name) values (?)";
  db.query(sql, [name_insert], (err, result) => {
    if (err) throw err;
    console.log(result.insertId);
    res.json({ id: result.insertId, name: name_insert });
  });
});

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});
