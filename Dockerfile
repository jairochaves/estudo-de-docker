FROM node:14.16-alpine

WORKDIR /app

COPY app /app

RUN npm install

EXPOSE 3000

CMD node server.js
